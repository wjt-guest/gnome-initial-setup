Source: gnome-initial-setup
Section: gnome
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: @GNOME_TEAM@
Build-Depends: debhelper-compat (= 12),
               dh-sequence-gnome,
               geoclue-2.0 (>= 2.3.1),
               itstool,
               libaccountsservice-dev,
               libcheese-gtk-dev (>= 3.28),
               libfontconfig1-dev,
               libgdm-dev (>= 3.8.3),
               libgeoclue-2-dev (>= 2.3.1),
               libgeocode-glib-dev,
               libglib2.0-dev (>= 2.63.1),
               libgnome-desktop-3-dev (>= 3.7.5),
               libgoa-1.0-dev,
               libgoa-backend-1.0-dev,
               libgtk-3-dev (>= 3.19.12),
               libgweather-3-dev (>= 3.13.91),
               libibus-1.0-dev (>= 1.5.2),
               libjson-glib-dev,
               libkrb5-dev,
               libnm-dev (>= 1.2),
               libnma-dev (>= 1.0),
               libpango1.0-dev (>= 1.32.5),
               libpolkit-gobject-1-dev (>= 0.103),
               libpwquality-dev,
               librest-dev,
               libsecret-1-dev (>= 0.18.8),
               libsystemd-dev [linux-any],
               libwebkit2gtk-4.0-dev,
               meson (>= 0.47.0)
Rules-Requires-Root: no
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/gnome-team/gnome-initial-setup
Vcs-Git: https://salsa.debian.org/gnome-team/gnome-initial-setup.git
Homepage: https://git.gnome.org/browse/gnome-initial-setup/

Package: gnome-initial-setup
Architecture: linux-any
Depends: ${shlibs:Depends},
         policykit-1 (>= 0.103),
         adduser,
         gnome-settings-daemon (>= 3.24),
         ${logo:Depends},
         ${misc:Depends}
Recommends: gnome-getting-started-docs,
            gnome-keyring,
Suggests: gdm3
Description: Initial GNOME system setup helper
 After acquiring or installing a new system there are a few essential things
 to set up before use. GNOME Initial Setup aims to provide a simple, easy,
 and safe way to prepare a new system.
 .
 GNOME Initial Setup runs the first time you log in to the GNOME desktop
 and lets you easily configure your language, keyboard layout, online accounts
 integration, and more.
 .
 If you want to configure these things at any other time, run the Settings app.
